Third Party Software
====================

eric7 includes copies of these third party packages due to several reasons.

| Name         |  Version  | License                     |
|:------------:|:---------:|:----------------------------|
| eradicate    |   2.3.0   | MIT License (Expat License) |
| mccabe       |   0.7.0   | MIT License (Expat License) |
| pip-licenses |   4.4.0   | MIT License (MIT)           |
| pycodestyle  |   2.11.1  | MIT License (Expat License) |
| pyflakes     |   3.2.0   | MIT License (MIT)           |
|              |           |                             |
| jquery       |   3.7.1   | MIT License (MIT)           |
| jquery-ui    |  1.13.2   | MIT License (MIT)           |

Some code in eric7 was adapted from these third party packages.

| Name                          |  Version  | License                            |
|:-----------------------------:|:---------:|:-----------------------------------|
| bandit                        |   1.7.8   | Apache License 2.0                 |
| flake8-alphabetize            |   0.0.21  | MIT License (MIT No Attribution)   |
| flake8-annotations            |   3.1.1   | MIT License (MIT)                  |
| flake8-annotations-complexity |   0.0.8   | MIT License (MIT)                  |
| flake8-annotations-coverage   |   0.0.6   | MIT License (MIT)                  |
| flake8-async                  |  22.11.14 | MIT License (MIT)                  |
| flake8-bugbear                |  24.4.26  | MIT License (MIT)                  |
| flake8-comprehensions         |   3.14.0  | MIT License (MIT)                  |
| flake8-future-annotations     |   1.1.0   | MIT License (MIT)                  |
| flake8-implicit-str-concat    |   0.4.0   | MIT License (MIT)                  |
| flake8-local-import           |   1.0.6   | MIT License (MIT)                  |
| flake8-logging                |   1.6.0   | MIT License (MIT)                  |
| flake8-pep585                 |   0.1.7   | Mozilla Public License Version 2.0 |
| flake8-pep604                 |   1.1.0   | MIT License (MIT)                  |
| flake8-simplify               |   0.21.0  | MIT License (MIT)                  |
| flake8-tidy-imports           |   4.10.0  | MIT License (MIT)                  |
| flake8-unused-arguments       |   0.0.13  | MIT License (MIT)                  |
| flake8-unused-globals         |   0.1.10  | MIT License (MIT)                  |
| flake8-use-pathlib            |   0.3.0   | MIT License (MIT)                  |
