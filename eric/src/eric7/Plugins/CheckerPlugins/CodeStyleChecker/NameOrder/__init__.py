# -*- coding: utf-8 -*-

# Copyright (c) 2023 - 2024 Detlev Offenbach <detlev@die-offenbachs.de>
#

"""
Package implementing a checker for the name order of import statements and handled
exceptions.
"""
