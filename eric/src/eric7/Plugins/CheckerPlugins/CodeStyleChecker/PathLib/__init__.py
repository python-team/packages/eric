# -*- coding: utf-8 -*-

# Copyright (c) 2021 - 2024 Detlev Offenbach <detlev@die-offenbachs.de>
#

"""
Package implementing the checker for functions that can be replaced by use of
the pathlib module.
"""
