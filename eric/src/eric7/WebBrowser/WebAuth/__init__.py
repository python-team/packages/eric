# -*- coding: utf-8 -*-

# Copyright (c) 2024 Detlev Offenbach <detlev@die-offenbachs.de>
#

"""
Package implementing dialogs for the WebAuth flow and security key management.
"""
