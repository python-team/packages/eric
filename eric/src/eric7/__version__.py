# -*- coding: utf-8 -*-

# Copyright (c) 2024 Detlev Offenbach <detlev@die-offenbachs.de>
#

"""
Module containing some eric-ide version information.
"""

Version = "24.8 (rev. 8f58c95bc6a8)"
VersionOnly = "24.8"
