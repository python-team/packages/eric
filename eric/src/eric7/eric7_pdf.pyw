# -*- coding: utf-8 -*-

# Copyright (c) 2023 - 2024 Detlev Offenbach <detlev@die-offenbachs.de>
#

"""
Module implementing the Windows entry point.
"""

if __name__ == "__main__":
    from eric7_pdf import main

    main()
