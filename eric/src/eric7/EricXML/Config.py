# -*- coding: utf-8 -*-

# Copyright (c) 2004 - 2024 Detlev Offenbach <detlev@die-offenbachs.de>
#

"""
Module implementing some common configuration stuff for the XML package.
"""

# version number of the plugin repository file
pluginRepositoryFileFormatVersion = "4.2"

# version number of the web browser spell check dictionaries list file
dictionariesListFileFormatVersion = "1.0"
