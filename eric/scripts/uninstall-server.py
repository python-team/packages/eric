#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2024 Detlev Offenbach <detlev@die-offenbachs.de>
#
# This is the uninstall script for the eric-ide server.
#

"""
Uninstallation script for the eric-ide server.
"""

import argparse
import contextlib
import os
import shutil
import sys
import sysconfig

# Define the globals.
currDir = os.getcwd()
scriptsDir = None
modDir = None
pyModDir = None
installPackage = "eric7"


def exit(rcode=0):
    """
    Exit the install script.

    @param rcode result code to report back
    @type int
    """
    global currDir

    if sys.platform.startswith("win"):
        with contextlib.suppress(EOFError):
            input("Press enter to continue...")  # secok

    os.chdir(currDir)

    sys.exit(rcode)


def initGlobals():
    """
    Module function to set the values of globals that need more than a
    simple assignment.
    """
    global modDir, pyModDir, scriptsDir

    # determine the platform scheme
    if sys.platform.startswith(("win", "cygwin")):
        scheme = "nt_user"
    elif sys.platform == "darwin":
        scheme = "osx_framework_user"
    else:
        scheme = "posix_user"

    # determine modules directory
    modDir = sysconfig.get_path("platlib")
    if not os.access(modDir, os.W_OK):
        # can't write to the standard path, use the 'user' path instead
        modDir = sysconfig.get_path("platlib", scheme)
    pyModDir = modDir

    # determine the scripts directory
    scriptsDir = sysconfig.get_path("scripts")
    if not os.access(scriptsDir, os.W_OK):
        # can't write to the standard path, use the 'user' path instead
        scriptsDir = sysconfig.get_path("scripts", scheme)


def wrapperNames(dname, wfile):
    """
    Create the platform specific names for the wrapper script.

    @param dname name of the directory to place the wrapper into
    @type str
    @param wfile basename (without extension) of the wrapper script
    @type str
    @return list of names of the wrapper scripts
    @rtype list of str
    """
    wnames = (
        [dname + "\\" + wfile + ".cmd", dname + "\\" + wfile + ".bat"]
        if sys.platform.startswith(("win", "cygwin"))
        else [dname + "/" + wfile]
    )

    return wnames


def uninstallEricServer():
    """
    Uninstall the old eric-ide server files.
    """
    global installPackage, pyModDir, scriptsDir

    try:
        # Cleanup the package directories
        dirname = os.path.join(pyModDir, installPackage)
        if os.path.exists(dirname):
            shutil.rmtree(dirname, ignore_errors=True)
    except OSError as msg:
        sys.stderr.write("Error: {0}\nTry uninstall with admin rights.\n".format(msg))
        exit(7)

    # Remove the wrapper scripts
    rem_wnames = ["eric7_server"]
    try:
        for rem_wname in rem_wnames:
            for rwname in wrapperNames(scriptsDir, rem_wname):
                if os.path.exists(rwname):
                    os.remove(rwname)
    except OSError as msg:
        sys.stderr.write("Error: {0}\nTry uninstall with admin rights.\n".format(msg))
        exit(7)


def createArgumentParser():
    """
    Function to create an argument parser.

    @return created argument parser object
    @rtype argparse.ArgumentParser
    """
    parser = argparse.ArgumentParser(description="Uninstall eric-ide server.")
    return parser


def main():
    """
    The main function of the script.
    """
    initGlobals()

    parser = createArgumentParser()
    parser.parse_args()

    print("\nUninstalling eric-ide server ...")
    uninstallEricServer()
    print("\nUninstallation complete.")
    print()

    exit(0)


if __name__ == "__main__":
    try:
        main()
    except SystemExit:
        raise
    except Exception:
        print(
            """An internal error occured.  Please report all the output"""
            """ of the program,\nincluding the following traceback, to"""
            """ eric-bugs@eric-ide.python-projects.org.\n"""
        )
        raise

#
# eflag: noqa = M801
