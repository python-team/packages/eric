#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2016 - 2024 Detlev Offenbach <detlev@die-offenbachs.de>
#
# This is the uninstall script for the eric debug client.
#

"""
Uninstallation script for the eric debug clients.
"""

import argparse
import contextlib
import os
import shutil
import sys
import sysconfig

# Define the globals.
currDir = os.getcwd()
modDir = None
pyModDir = None
installPackage = "eric7"


def exit(rcode=0):
    """
    Exit the install script.

    @param rcode result code to report back
    @type int
    """
    global currDir

    if sys.platform.startswith("win"):
        with contextlib.suppress(EOFError):
            input("Press enter to continue...")  # secok

    os.chdir(currDir)

    sys.exit(rcode)


def initGlobals():
    """
    Module function to set the values of globals that need more than a
    simple assignment.
    """
    global modDir, pyModDir

    modDir = sysconfig.get_path("platlib")
    if not os.access(modDir, os.W_OK):
        # can't write to the standard path, use the 'user' path instead
        if sys.platform.startswith(("win", "cygwin")):
            scheme = "nt_user"
        elif sys.platform == "darwin":
            scheme = "osx_framework_user"
        else:
            scheme = "posix_user"
        modDir = sysconfig.get_path("platlib", scheme)
    pyModDir = modDir


def uninstallEricDebugClients():
    """
    Uninstall the old eric debug client files.
    """
    global pyModDir

    try:
        # Cleanup the install directories
        dirname = os.path.join(pyModDir, installPackage)
        if os.path.exists(dirname):
            shutil.rmtree(dirname, ignore_errors=True)
    except OSError as msg:
        sys.stderr.write("Error: {0}\nTry uninstall with admin rights.\n".format(msg))
        exit(7)


def createArgumentParser():
    """
    Function to create an argument parser.

    @return created argument parser object
    @rtype argparse.ArgumentParser
    """
    parser = argparse.ArgumentParser(description="Uninstall eric7 debug client.")
    return parser


def main():
    """
    The main function of the script.
    """
    initGlobals()

    parser = createArgumentParser()
    parser.parse_args()

    print("\nUninstalling eric debug clients ...")
    uninstallEricDebugClients()
    print("\nUninstallation complete.")
    print()

    exit(0)


if __name__ == "__main__":
    try:
        main()
    except SystemExit:
        raise
    except Exception:
        print(
            """An internal error occured.  Please report all the output"""
            """ of the program,\nincluding the following traceback, to"""
            """ eric-bugs@eric-ide.python-projects.org.\n"""
        )
        raise

#
# eflag: noqa = M801
